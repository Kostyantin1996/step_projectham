let titles = document.querySelectorAll('.services_item');

for (let i = 0; i < titles.length; i++) {
    titles[i].addEventListener('click', (event) => {
        let oldActive = document.querySelector('.services_item.active');
        oldActive.classList.remove('active');
        event.target.classList.add('active');

        let activeContent = document.querySelector('[data-active-content]');
        let contents = document.querySelectorAll('.services_content');
        activeContent.removeAttribute('data-active-content');
        let content = contents[i];
        content.setAttribute('data-active-content', '');
    })
}

let workGalleryItems =  [
{
    src: "img/graphic design/graphic-design1.jpg",
    category: "Graphic Design",
},
{
    src: "img/web design/web-design1.jpg",
    category: "Web Design",
},
{
    src: "img/wordpress/wordpress5.jpg",
    category: "Wordpress",
},
{
    src: "img/wordpress/wordpress6.jpg",
    category: "Wordpress",
},
{
    src: "img/web design/web-design4.jpg",
    category: "Web Design",
},
{
    src: "img/graphic design/graphic-design4.jpg",
    category: "Graphic Design",
},
{
    src: "img/web design/web-design5.jpg",
    category: "Web Design",
},
{
    src: "img/web design/web-design2.jpg",
    category: "Web Design",
},
{
    src: "img/graphic design/graphic-design5.jpg",
    category: "Graphic Design",
},
{
    src: "img/landing page/landing-page5.jpg",
    category: "Landing Pages",
},
{
    src: "img/web design/web-design3.jpg",
    category: "Web Design",
},
{
    src: "img/graphic design/graphic-design2.jpg",
    category: "Graphic Design",
},
{
    src: "img/landing page/landing-page3.jpg",
    category: "Landing Pages",
},
{
    src: "img/wordpress/wordpress4.jpg",
    category: "Wordpress",
},
{
    src: "img/graphic design/graphic-design3.jpg",
    category: "Graphic Design",
},
{
    src: "img/web design/web-design6.jpg",
    category: "Web Design",
},
{
    src: "img/graphic design/graphic-design6.jpg",
    category: "Graphic Design",
},
{
    src: "img/landing page/landing-page1.jpg",
    category: "Landing Pages",
},
{
    src: "img/landing page/landing-page2.jpg",
    category: "Landing Pages",
},
{
    src: "img/wordpress/wordpress2.jpg",
    category: "Wordpress",
},
{
    src: "img/wordpress/wordpress3.jpg",
    category: "Wordpress",
},
{
    src: "img/landing page/landing-page6.jpg",
    category: "Landing Pages",
},
{
    src: "img/wordpress/wordpress1.jpg",
    category: "Wordpress",
},
{
    src: "img/landing page/landing-page4.jpg",
    category: "Landing Pages",
},

];

function setWorkGalleryItem(item) {
    let src = item.src;
    let category = item.category;

    let workGallery = document.querySelector(".work_gallery");

    let div = document.createElement("div");
    div.classList.add("work_gallery_item");
    workGallery.append(div);

    let img = document.createElement("img");
    img.setAttribute("src", src);
    div.append(img);

    let divHoverDesign = document.createElement("div");
    divHoverDesign.classList.add("hover_design");
    div.append(divHoverDesign);

    let iconImg = document.createElement("img");
    iconImg.setAttribute("src", "img/icon.svg");
    divHoverDesign.append(iconImg);

    let h3 = document.createElement("h3");
    h3.classList.add("hover_title");
    h3.innerText = "creative design";
    divHoverDesign.append(h3);

    let p = document.createElement("p");
    p.classList.add("hover_text");
    p.innerText = category;
    divHoverDesign.append(p);
}

document.addEventListener('DOMContentLoaded', function () {
    for(let i = 0; i < 12; i++){
        setWorkGalleryItem(workGalleryItems[i]);
    }

    for(let i = 0; i <9; i ++){
        writeGalleryItems(gallery[i]);
    }
    
    const slider = ItcSlider.getOrCreateInstance('.itc-slider');
})

let workList = document.querySelector('.work_list');
let workBtn  = document.getElementById('load_more');

workList.addEventListener('click', (event) => {
    if(event.target.tagName === 'LI' && !event.target.classList.contains('active')) {
        let oldActive = document.querySelector('.work_item.active');
        oldActive.classList.remove('active');

        event.target.classList.add('active');

        let activeCategory = event.target.dataset.addedCategory;

        let showWorkBtn = addCategoryItems(activeCategory, 12);

        if (showWorkBtn) {
        workBtn.style.display = 'initial';
        } else {
        workBtn.style.display = 'none';
        }
    }
})

function addCategoryItems (category, count) {
    clearWorkGallery();

    let actualItems = workGalleryItems.filter((elem) => category === 'All' || elem.category === category).slice(0, count);
    
    for(let item of actualItems) {
        setWorkGalleryItem(item);
    }
    
    let showWorkBtn = actualItems.length >= count
    return showWorkBtn;
}

workBtn.addEventListener('click', () => {
    let activeItem = document.querySelector('.work_item.active');
    let activeCategory = activeItem.dataset.addedCategory;

    addCategoryItems(activeCategory);

    workBtn.style.display = 'none';

})

function clearWorkGallery () {
    let workGallery = document.querySelector('.work_gallery');
    workGallery.innerHTML = "";
}

let gallery = [
    {
        src: "img/ia-Pools-1 1.png",
    },
    {
        src: "img/Kids-Store-Lighting-1 (1) 1.png",
    },
    {
        src: "img/vanglo-house-6 2.png",
    },
    {
        src: "img/Kids-Store-Lighting-2 2.png",
    },
    {
        src: "img/vanglo-house-4 1.png",
    },
    {
        src: "img/IA-Hourses-6 1.png",
    },
    {
        src: "img/vanglo-house-1 3.png",
    },
    {
        src: "img/IA-billionares-6 1.png",
    },
    {
        src: "img/Brazil-staduims-IA-1 1.png",
    },
];

function writeGalleryItems (item) {
    let src = item.src;

    let gallery = document.querySelector(".second_inner_section");
    let div = document.createElement("div");
    div.classList.add("second_inner_section_item");
    gallery.append(div);

    let img = document.createElement("img");
    img.setAttribute("src", src);
    div.append(img);

    let galleryHover = document.createElement("div");
    galleryHover.classList.add("second_hover_section");
    div.append(galleryHover);

    let firstIconImg = document.createElement("img");
    firstIconImg.setAttribute("src", "img/search.png");
    galleryHover.append(firstIconImg);

    let secondIconImg = document.createElement("img");
    secondIconImg.setAttribute("src", "img/zoom.png");
    galleryHover.append(secondIconImg);
}

let slider = document.querySelector('.itc-slider');

slider.addEventListener("click", (event) => {
    let move = 0;

    if (event.target.dataset.slide) {
        if (event.target.dataset.slide === 'prev') {
            move = -1;
        } else {
            move = 1;
        }

        let length = document.querySelectorAll('.itc-slider__items>*').length;

        let oldActive = document.querySelector('.itc-slider__item.active');
        oldActive.classList.remove('active');
        
        let index = +oldActive.dataset.index;
        index = ((index + move) + length) % length;

        let titles = Array.from(document.querySelectorAll(`.itc-slider__item`));
        let activeTitle = titles.filter(item => item.dataset.index === `${index}`)[0];
        activeTitle.classList.add("active");

        let activeContent = document.querySelector('[data-active-person]');
        activeContent.removeAttribute('data-active-person');

        let contents = document.querySelectorAll('.selected_person');
        let content = contents[index];
        content.setAttribute('data-active-person', '');

    }
})



let sliderItems = document.querySelectorAll('.itc-slider__item');

for(let i = 0; i < sliderItems.length; i++) {
    sliderItems[i].addEventListener('click', (event) => {
        let oldActive = document.querySelector('.itc-slider__item.active');
        oldActive.classList.remove('active');

        let div = event.target.closest('div');
        div.classList.add('active');

        let selectedPerson = document.querySelector('[data-active-person]');
        selectedPerson.removeAttribute('data-active-person');
        
        let persons = document.querySelectorAll('.selected_person');
        let person = persons[i];
        person.setAttribute('data-active-person', '');
    })
}

